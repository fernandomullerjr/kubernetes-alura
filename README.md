## Kubernetes-Alura

Projeto voltado ao curso de Kubernetes da Alura(Kubernetes: Pods, Services e ConfigMaps).

- [ ] [Conhecendo o Kubernetes](https://cursos.alura.com.br/course/kubernetes-pods-services-configmap/section/10894/tasks)
- [ ] [Criando o cluster](https://cursos.alura.com.br/course/kubernetes-pods-services-configmap/section/10895/tasks)
- [ ] [Criando e entendendo pods](https://cursos.alura.com.br/course/kubernetes-pods-services-configmap/section/10896/tasks)
- [ ] [Expondo pods com services](https://cursos.alura.com.br/course/kubernetes-pods-services-configmap/section/10897/tasks)
- [ ] [Aplicando services ao projeto](https://cursos.alura.com.br/course/kubernetes-pods-services-configmap/section/10898/tasks)
- [ ] [Definindo variáveis de ambiente](https://cursos.alura.com.br/course/kubernetes-pods-services-configmap/section/10899/tasks)

***
